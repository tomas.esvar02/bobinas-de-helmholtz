include<parametros.scad>


$fn=80;

module bobina(d=diametro_bobina, h=wall){
    difference(){
      cylinder(d=d, h=h*2, center=true);
      rotate_extrude(convexity = 10)
        translate([d/2, 0, 0]) circle(d=h);
      cylinder(d=d-h*2, h=h*3, center=true);
    }
}

bobina();