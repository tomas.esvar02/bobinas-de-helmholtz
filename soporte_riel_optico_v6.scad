use<Bobina_Hemholzt.scad>
use<utilities.scad>
include<parametros.scad>


$fn=80;



//Parametros

module trapezio (){
       polygon([[-ancho_soporte/2,0],[ancho_soporte/2,0],[ancho_arriba,alto_soporte],[-ancho_arriba,alto_soporte]]);

}


rotate([90,0,0]){
    
    //bobina de muestra
    *translate([0,diametro_bobina/2+radio_pared,extrude_largo/2]){
            rotate([0,90,0]){
                bobina(d=diametro_bobina, h=wall);
            }
        }
    
    difference(){
        //Base trapezio extruído
        linear_extrude(extrude_largo){
                minkowski(){
                    trapezio();
                    circle(r=radio_pared);
                }
            }
        //Objetos resta    
        //Trapezio interior    
        translate([0,0,radio_pared]){
            linear_extrude(extrude_largo-(2*radio_pared)){
            trapezio();
            }    
        }
        
        //Caladuras exteriores
        reflect([1,0,0]){
            translate([ancho_arriba+radio_pared,0,(extrude_largo-largo_cubo)/2]){
                cube([(ancho_soporte/2-ancho_arriba-radio_pared),alto_soporte,largo_cubo]);  
            }
        }
        
        //Apoyo de la bobina
        translate([0,diametro_bobina/2+radio_pared,extrude_largo/2]){
            rotate([0,90,0]){
                hull() bobina(d=diametro_bobina, h=wall);
            }
        }
        
        //Orificios Bornera
        reflect([1,0,0]){
            translate([-((ancho_soporte/2)-x_bornera),altura_bornera,extrude_largo/2]){
                reflect([0,0,1]){
                    translate([0,0,loc_bornera+largo_cubo/2]){
                        rotate([-90,0,alfa]){        
                            cylinder(r=radio_bornera,h=radio_pared*1.01);
                        }
                    }
                }
           }
        }
        
        // nota in-out

        translate([0.8*radio_pared+((ancho_soporte/2)-(altura_bornera+s_text)/(tan(alfa))),altura_bornera+5,loc_bornera+s_text/2]){
            rotate([0,90,90-alfa]){
                translate([0,0,radio_pared/4]){
                    linear_extrude(1)text("in",size=s_text);
                }
            }
        }
       translate([0.8*radio_pared+((ancho_soporte/2)-(altura_bornera+s_text)/(tan(alfa))),altura_bornera+5,extrude_largo+s_text-loc_bornera]){
            rotate([0,90,90-alfa]){
               translate([0,0,radio_pared/4]){    
                    linear_extrude(1)text("out",size=s_text);
                }
            }
        }

    

        
        //Orificio anclaje nuez
        
        reflect([1,0,0]){
            translate([ancho_soporte*3/8, 0, extrude_largo/2]){
                rotate([90,0,0]){
                    trylinder_selftap(3.25, h=999, center=true);
                }
            }
        }
    }


    
    //Paredes sostén
    difference(){
        reflect([1,0,0]){
      //      translate([wall+radio_pared/2,0,(extrude_largo-largo_cubo)/2]){
      translate([wall,0,(extrude_largo-largo_cubo)/2]){
                cube([radio_pared,alto_soporte,largo_cubo]);  
            }
        }
        
        //Orificio para prensar la bobina
        translate([0, alto_soporte/3, extrude_largo/2]) rotate([90,0,90]) trylinder_selftap(3, h=90, center=true);
    }
}



