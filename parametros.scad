// Bobinas
diametro_bobina = 180;
wall = 10;

//valores usados en soporte de bobinas 
radio_pared = 3;
extrude_largo = 50;
alto_soporte = 20;
ancho_soporte = 70;

ancho_arriba = (wall+radio_pared);
largo_cubo = extrude_largo/3;
loc_bornera = (extrude_largo-largo_cubo)/4;
altura_bornera = alto_soporte/2;
radio_bornera = 3;
//size texto
s_text = 5;

//angulo de inclinacion de las paredes inclinadas del soporte de las bobinas
alfa = acos(((ancho_soporte/2)-ancho_arriba)/(sqrt((-ancho_arriba+ancho_soporte/2)^2+(alto_soporte)^2)));
x_bornera = altura_bornera/(tan(alfa));
echo(x_bornera);


// valores usados en soporte de brujula

cubo_base_bru = diametro_bobina/2-ancho_soporte ;
alto_base_bru = 3;

altura_brujula = 10;
radio_brujula = 30;

escalax=1.5;
escalay=0.9;
escalaz=0.9;

cubo_base_altura = 20;



