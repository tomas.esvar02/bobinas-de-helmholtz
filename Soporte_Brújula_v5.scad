use<Bobina_Hemholzt.scad>
use<utilities.scad>
include<parametros.scad>


$fn=80;

//muestra del soporte de la bobina

/*module trapezio (){
       polygon([[-ancho_soporte/2,0],[ancho_soporte/2,0],[ancho_arriba,alto_soporte],[-ancho_arriba,alto_soporte]]);
}
*/


//Dirección del riel en el eje x
//Base


/*translate([0.25,0.25,alto_base_bru*1.7]){
    difference(){
        cube([cubo_base_bru-0.5,cubo_base_bru-0.5,diametro_bobina/2-alto_base_bru-4]);
        translate([1.75,1.75,0]){
            cube([cubo_base_bru-4,cubo_base_bru-4,diametro_bobina/2-alto_base_bru-4]);
        }
    }
}*/
difference(){
    
    union(){
        cube([cubo_base_bru,extrude_largo,alto_base_bru],center=true);
        translate([0,0,cubo_base_altura/2]){
            cube([cubo_base_bru-radio_pared-0.4,cubo_base_bru-radio_pared-0.4,cubo_base_altura],center=true);
        }
    }
    
    translate([0, 0, cubo_base_altura/2]){
        cube([cubo_base_bru-radio_pared*2-0.4,cubo_base_bru-radio_pared*2-0.4,cubo_base_altura],center=true);
    }
    
    translate([0,0,0]){
         trylinder_selftap(3.25, h=20, center=true);
    }    
    
}
